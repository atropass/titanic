import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def extract_title(name):
    for title in ['Mr.', 'Mrs.', 'Miss.']:
        if title in name:
            return title
    return None


def get_filled():
    df = get_titatic_dataframe()  
    df['Title'] = df['Name'].apply(extract_title)
    df = df[df['Title'].isin(['Mr.', 'Mrs.', 'Miss.'])]
    median_ages = df.groupby('Title')['Age'].median().round().astype('Int64')
    missing_ages = df[df['Age'].isnull()]['Title'].value_counts()
    result = [
        ('Mr.', missing_ages.get('Mr.', 0), median_ages.get('Mr.')),
        ('Mrs.', missing_ages.get('Mrs.', 0), median_ages.get('Mrs.')),
        ('Miss.', missing_ages.get('Miss.', 0), median_ages.get('Miss.'))
    ]

    return result
